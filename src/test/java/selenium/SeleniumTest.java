package selenium;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SeleniumTest {
    WebDriver driver; //browser

    @BeforeAll
    static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeEach
    void setupTest() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
    }

    @AfterEach
    void teardown() {
        //driver.quit();
    } //zavre prohlizec

    @Test
    void prihlasovaci_obrazovka() {
        driver.get("https://link.springer.com/");
        driver.findElement(By.xpath("//*[@id=\"header\"]/div[1]/div[1]/a")).click();
    }

    @Test
    void pokrocile_vyhledavani(){
        driver.get("https://link.springer.com/");
        driver.findElement(By.xpath("//*[@id=\"search-options\"]/button/img")).click();
        //id = advanced-search-link
        driver.findElement(By.xpath("//*[@id=\"advanced-search-link\"]")).click();
    }

    @Test
    void advanced_search(){
        driver.get("https://link.springer.com/advanced-search");
        driver.findElement(By.xpath("//*[@id=\"all-words\"]")).sendKeys("molecular");
        // //*[@id="exact-phrase"]
        driver.findElement(By.xpath("//*[@id=\"exact-phrase\"]")).sendKeys("Molecular Imaging and " +
                "Biology presents original" +
                "research contributions on the " +
                "utilization of molecular imaging in problems of relevance in biology and medicine.");
        // //*[@id="least-words"]
        driver.findElement(By.xpath("//*[@id=\"least-words\"]")).sendKeys("medicine");
        // //*[@id="without-words"]
        driver.findElement(By.xpath("//*[@id=\"without-words\"]")).sendKeys("amogus");
        // //*[@id="title-is"]
        driver.findElement(By.xpath("//*[@id=\"title-is\"]")).sendKeys("Molecular Imaging and Biology");
        // Jason S. Lewis
        // //*[@id="author-is"]
        driver.findElement(By.xpath("//*[@id=\"author-is\"]")).sendKeys("Lewis");
        // //*[@id="facet-start-year"] - 2005
        driver.findElement(By.xpath("//*[@id=\"facet-start-year\"]")).sendKeys("2005");
        // //*[@id="facet-end-year"] - 2023
        driver.findElement(By.xpath("//*[@id=\"facet-end-year\"] ")).sendKeys("2023");
        // //*[@id="submit-advanced-search"]
        driver.findElement(By.xpath("//*[@id=\"submit-advanced-search\"]")).click();
    }

    @Test
    void prochazeni_vysledku(){ //print titles of the search result???

        driver.get("https://link.springer.com/search");
        driver.findElement(By.xpath("//*[@id=\"query\"]")).sendKeys("Molecular");
        // //*[@id="search"]
        driver.findElement(By.xpath("//*[@id=\"search\"]")).click();

        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;

        //cookies
        // /html/body/section/div/div[2]/button[1]
        driver.findElement(By.xpath("/html/body/section/div/div[2]/button[1]")).click();

        // Print titles of the search results
        List<WebElement> content_items = driver.findElements(By.className("title"));

        for (WebElement contentItem : content_items) {
            System.out.println(contentItem.getText());
        }

        //driver.quit();

    }

    @Test
    void prihlaseni(){
        driver.get("https://link.springer.com/signup-login");

        //login - //*[@id="login-box-email"]
        driver.findElement(By.xpath("//*[@id=\"login-box-email\"]")).sendKeys("abcd@gmail.com");

        //pass - //*[@id="login-box-pw"]
        driver.findElement(By.xpath("//*[@id=\"login-box-pw\"]")).sendKeys("password");

        //submit - //*[@id="login-box"]/div/div[3]/button
        driver.findElement(By.xpath("//*[@id=\"login-box\"]/div/div[3]/button")).click();
    }

    @Test
    void read_the_article() { //read the article mean scroll the article??

        //https://link.springer.com/article/10.1007/s40868-022-00121-2
        driver.get("https://link.springer.com/article/10.1007/s40868-022-00121-2");
        System.out.println("****************************************************************");

        // //*[@id="main-content"]/main/article/div[1]/header/h1
        WebElement name_of_article = driver.findElement(By.xpath("//*[@id=\"main-content\"]/main/article/div[1]/header/h1"));
        String articleName = name_of_article.getText();
        System.out.println("Name of the article: " + articleName);


        WebElement aboutthisarticleSection = driver.findElement(By.xpath("//*[@id=\"rc-sec-article-info\"]/a"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", aboutthisarticleSection);

        //cookies
        // /html/body/section/div/div[2]/button[1]
        driver.findElement(By.xpath("/html/body/section/div/div[2]/button[1]")).click();

        //Clicking on the article parts (reading)
        // //*[@id="rc-sec-Abs1"]/a
        driver.findElement(By.xpath("//*[@id=\"rc-sec-Abs1\"]/a")).click();


        // //*[@id="rc-sec-Sec1"]/a
        driver.findElement(By.xpath("//*[@id=\"rc-sec-Sec1\"]/a")).click();
        // //*[@id="rc-sec-Sec3"]/a
        driver.findElement(By.xpath("//*[@id=\"rc-sec-Sec3\"]/a")).click();
        // //*[@id="rc-sec-Sec4"]/a
        driver.findElement(By.xpath("//*[@id=\"rc-sec-Sec4\"]/a")).click();
        // //*[@id="rc-sec-Sec7"]/a
        driver.findElement(By.xpath("//*[@id=\"rc-sec-Sec7\"]/a")).click();
        // //*[@id="rc-sec-Sec10"]/a
        driver.findElement(By.xpath("//*[@id=\"rc-sec-Sec10\"]/a")).click();
        // //*[@id="rc-sec-Bib1"]/a
        driver.findElement(By.xpath("//*[@id=\"rc-sec-Bib1\"]/a")).click();
        // //*[@id="rc-sec-author-information"]/a
        driver.findElement(By.xpath("//*[@id=\"rc-sec-author-information\"]/a")).click();
        // //*[@id="rc-sec-additional-information"]/a
        driver.findElement(By.xpath("//*[@id=\"rc-sec-additional-information\"]/a")).click();
        // //*[@id="rc-sec-rightslink"]/a
        driver.findElement(By.xpath("//*[@id=\"rc-sec-rightslink\"]/a")).click();
        // //*[@id="rc-sec-article-info"]/a
        driver.findElement(By.xpath("//*[@id=\"rc-sec-article-info\"]/a")).click();


        /*
        //About this article
        //*[@id="article-info-content"]/div/div[2]/p[1]/text()[1]
        WebElement about_this_article = driver.findElement(By.id("//*[@id=\"article-info-content\"/div/div[2]/p[1]/text()[1]"));
        String articleAbout = about_this_article.getText();

        System.out.println("****************************************************************");
        System.out.println("About this article: " + articleAbout); */

    }

    @Test
    void posledni_ukol_DOI(){
        //advanced search
        driver.get("https://link.springer.com/");
        driver.findElement(By.xpath("//*[@id=\"search-options\"]/button/img")).click();
        driver.findElement(By.xpath("//*[@id=\"advanced-search-link\"]")).click();

        //advanced-search-form
        driver.findElement(By.xpath("//*[@id=\"all-words\"]")).sendKeys("Page Object Model");

        // //*[@id="least-words"]
        driver.findElement(By.xpath("//*[@id=\"least-words\"]")).sendKeys("Sellenium Testing");

        //checkbox(year)
        WebElement selectElement = driver.findElement(By.id("date-facet-mode"));
        Select select = new Select(selectElement);
        select.selectByValue("in");

        //year
        // //*[@id="facet-start-year"]
        driver.findElement(By.xpath("//*[@id=\"facet-start-year\"]")).sendKeys("2023");

        //cookies
        // /html/body/section/div/div[2]/button[1]
        driver.findElement(By.xpath("/html/body/section/div/div[2]/button[1]")).click();

        // //*[@id="submit-advanced-search"]
        // //*[@id="submit-advanced-search"]
        driver.findElement(By.xpath("//*[@id=\"submit-advanced-search\"]")).click();


        //article
        // //*[@id="content-type-facet"]/ol/li[4]/a/span[2]
        driver.findElement(By.xpath("//span[text()='Article']")).click();

        ////*[@id="results-list"]/li[2]
        ////*[@id="results-list"]/li[3]

        List<WebElement> content_items = new ArrayList<>();

        for (int i = 1; i <= 4; i++) {
            String xpath = "//*[@id='results-list']/li[" + i + "]";
            WebElement article = driver.findElement(By.xpath(xpath));
            content_items.add(article);
        }

        //WebElement contentItem : content_items)

        for (int i = 0; i < content_items.size(); i++)  {
            WebElement contentItem = content_items.get(i);

            WebElement titleElement = contentItem.findElement(By.className("title"));
            String title = titleElement.getText();
            System.out.println("Title: " + title);

            //year
            WebElement  year = contentItem.findElement(By.className("year"));
            String date = year.getText();
            System.out.println("Publication year: " + date);

            //driver.findElement(By.className("title")).click();

            /*
            //date
            WebElement infoElement = contentItem.findElement(By.className("u-visually-hidden"));
            String info = infoElement.getText();
            System.out.println("Publication year: " + info); */

        }

    }
}
